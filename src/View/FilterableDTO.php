<?php

namespace EventBlock\View;

use EventBlock\ResultFilter\BlockFilterContainer;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPaginationInterface;

class FilterableDTO
{
    /**
     * @var BlockFilterContainer[]
     */
    private $availableFilters;
    /**
     * @var array
     */
    private $entityToDisplay;

    /**
     * @var SlidingPaginationInterface
     */
    private $slidingPagination;

    /**
     * @var string
     */
    private $title = 'Events';

    /**
     * @param BlockFilterContainer[] $availableFilters
     * @param SlidingPaginationInterface $slidingPagination
     */
    public function __construct(
        array $availableFilters,
        SlidingPaginationInterface $slidingPagination
    ) {
        $this->availableFilters = $availableFilters;
        $this->entityToDisplay = $slidingPagination->getItems();
        $this->slidingPagination = $slidingPagination;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return BlockFilterContainer[]
     */
    public function getAvailableFilters(): array
    {
        return $this->availableFilters;
    }

    /**
     * @param BlockFilterContainer[] $availableFilters
     */
    public function setAvailableFilters(array $availableFilters): void
    {
        $this->availableFilters = $availableFilters;
    }

    /**
     * @return array
     */
    public function getEntityToDisplay(): iterable
    {
        return $this->entityToDisplay;
    }

    /**
     * @param array $entityToDisplay
     */
    public function setEntityToDisplay(array $entityToDisplay): void
    {
        $this->entityToDisplay = $entityToDisplay;
    }

    /**
     * @return SlidingPaginationInterface
     */
    public function getSlidingPagination(): SlidingPaginationInterface
    {
        return $this->slidingPagination;
    }

    /**
     * @param SlidingPaginationInterface $slidingPagination
     */
    public function setSlidingPagination(SlidingPaginationInterface $slidingPagination): void
    {
        $this->slidingPagination = $slidingPagination;
    }
}