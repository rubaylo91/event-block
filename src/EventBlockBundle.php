<?php

namespace EventBlock;

use EventBlock\DependencyInjection\ConfigExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use function dirname;

class EventBlockBundle extends Bundle
{
    public function getPath(): string
    {

        return dirname(__DIR__);
    }

    public function getContainerExtension()
    {
        return new ConfigExtension();
    }
}