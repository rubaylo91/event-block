<?php

namespace EventBlock\QueryFilter;

use Elastica\Query\BoolQuery;

interface BoolQueryFilterInterface
{
    public function applyFilter(BoolQuery $boolQuery): BoolQuery;
}