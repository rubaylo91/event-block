<?php

namespace EventBlock\Paginator;

use Elastica\Aggregation\AbstractAggregation;
use EventBlock\QueryBuilder\ElasticaQueryBuilder;
use EventBlock\QueryFilter\BoolQueryFilterInterface;
use EventBlock\Sort\SortingInterface;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use FOS\ElasticaBundle\Paginator\PaginatorAdapterInterface;
use FOS\ElasticaBundle\Paginator\RawPaginatorAdapter;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class EntityPaginator
{
    /**
     * @var ElasticaQueryBuilder
     */
    private $elasticaQueryBuilder;
    /**
     * @var TransformedFinder
     */
    private $transformedFinder;
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(
        ElasticaQueryBuilder $elasticaQueryBuilder,
        TransformedFinder $eventsFinder,
        PaginatorInterface $paginator
    ) {
        $this->elasticaQueryBuilder = $elasticaQueryBuilder;
        $this->transformedFinder = $eventsFinder;
        $this->paginator = $paginator;
    }

    /**
     * @param int $pageNumber
     * @param int $perPage
     * @param BoolQueryFilterInterface[] $queryFilters
     * @param SortingInterface|null $sorting
     * @return PaginationInterface
     */
    public function getEventPaginator(
        int $pageNumber,
        int $perPage,
        ?array $queryFilters,
        ?SortingInterface $sorting = null
    ): PaginationInterface {
        $query = $this->elasticaQueryBuilder->buildSearchQuery(
            $queryFilters,
            [],
            $sorting

        );
        $paginatorAdapter = $this->transformedFinder->createPaginatorAdapter($query);
        return $this->paginator->paginate(
            $paginatorAdapter,
            $pageNumber,
            $perPage
        );
    }

    /**
     * @param BoolQueryFilterInterface[] $queryFilters
     * @param AbstractAggregation[] $aggregations
     * @return PaginatorAdapterInterface|RawPaginatorAdapter
     */
    public function getRefineFilters(array $queryFilters, array $aggregations)
    {
        $query = $this->elasticaQueryBuilder->buildSearchQuery(
            $queryFilters,
            $aggregations
        );

        return $this->transformedFinder->createRawPaginatorAdapter($query);
    }
}