<?php

namespace EventBlock\Sort;

use Elastica\Query;

interface SortingInterface
{
    public function applySorting(Query $query): Query;
}