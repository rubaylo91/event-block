<?php

namespace EventBlock\ResultFilter\FactoryMethod;

use EventBlock\ResultFilter\BlockFilter;
use EventBlock\ResultFilter\BlockFilterContainer;

abstract class AbstractBlockFilterFactory implements FilterKeyProviderInterface, AggregationKeyProviderInterface
{

    /**
     * @param string $aggregationKey
     * @param array $aggregations
     * @param string $filterKey
     * @return BlockFilter[]
     */
    public function getEmptyBlockFiltersFromBucket(
        string $aggregationKey,
        array $aggregations,
        string $filterKey
    ): array {
        $returnArray = [];
        foreach ($aggregations[$aggregationKey]['buckets'] as $bucket) {
            $returnArray[] = new BlockFilter('', $bucket['key'], false, $filterKey);
        }
        return $returnArray;
    }

    /**
     * @param BlockFilter[] $blockFilter
     * @return BlockFilterContainer
     */
    abstract public function createBlockFilterContainer(array $blockFilter): BlockFilterContainer;

    /**
     * @param BlockFilter[] $blockFilters
     * @return int[]
     */
    protected function getUniqueIds(array $blockFilters): array
    {
        $ids = [];
        foreach ($blockFilters as $filter) {
            $ids[] = $filter->getId();
        }
        return $ids;
    }
}