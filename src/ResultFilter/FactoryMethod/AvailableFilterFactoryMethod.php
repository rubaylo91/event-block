<?php

namespace EventBlock\ResultFilter\FactoryMethod;


use EventBlock\ResultFilter\BlockFilterContainer;

class AvailableFilterFactoryMethod
{

    /**
     * @param array $aggregations
     * @param FilterFactoryContainerInterface $filterFactoryContainer
     * @return BlockFilterContainer[]
     */
    public function getBlockFilterContainer(
        array $aggregations,
        FilterFactoryContainerInterface $filterFactoryContainer
    ): array {
        $returnArray = [];
        foreach ($filterFactoryContainer->getFactories() as $blockFilterFactory) {
            $returnArray[] = $blockFilterFactory->createBlockFilterContainer(
                $blockFilterFactory->getEmptyBlockFiltersFromBucket(
                    $blockFilterFactory->getAggregationKey(),
                    $aggregations,
                    $blockFilterFactory->getFilterKey()
                )
            );
        }

        return $returnArray;
    }

}