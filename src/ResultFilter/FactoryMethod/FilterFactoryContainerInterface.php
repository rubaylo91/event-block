<?php

namespace EventBlock\ResultFilter\FactoryMethod;


interface FilterFactoryContainerInterface
{
    /**
     * @return AbstractBlockFilterFactory[]
     */
    public function getFactories(): array;

}