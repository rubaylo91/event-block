<?php

namespace EventBlock\ResultFilter\FactoryMethod;

interface FilterKeyProviderInterface
{
    public function getFilterKey(): string;
}