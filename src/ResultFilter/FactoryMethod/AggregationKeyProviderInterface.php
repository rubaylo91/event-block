<?php

namespace EventBlock\ResultFilter\FactoryMethod;

interface AggregationKeyProviderInterface
{
    public function getAggregationKey(): string;
}