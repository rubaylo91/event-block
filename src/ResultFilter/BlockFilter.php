<?php

namespace EventBlock\ResultFilter;

class BlockFilter
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var int
     */
    private $id;
    /**
     * @var bool
     */
    private $active;
    /**
     * @var string
     */
    private $filterKey;

    public function __construct(string $title, int $id, bool $active, string $filterKey)
    {

        $this->title = $title;
        $this->id = $id;
        $this->active = $active;
        $this->filterKey = $filterKey;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getFilterKey(): string
    {
        return $this->filterKey;
    }

    /**
     * @param string $filterKey
     */
    public function setFilterKey(string $filterKey): void
    {
        $this->filterKey = $filterKey;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}