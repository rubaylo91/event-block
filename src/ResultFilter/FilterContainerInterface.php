<?php

namespace EventBlock\ResultFilter;

interface FilterContainerInterface
{
    public function getTitle(): string;

    /**
     * @return BlockFilter[]
     */
    public function getElements(): array;
}