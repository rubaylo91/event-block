<?php

namespace EventBlock\ResultFilter;

class BlockFilterContainer implements FilterContainerInterface
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var BlockFilter[]
     */
    private $elements;

    /**
     * @param string $title
     * @param BlockFilter[] $elements
     * @return void
     */
    public function __construct(string $title, array $elements)
    {

        $this->title = $title;
        $this->elements = $elements;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return BlockFilter[]
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param BlockFilter[] $elements
     */
    public function setElements(array $elements): void
    {
        $this->elements = $elements;
    }

}