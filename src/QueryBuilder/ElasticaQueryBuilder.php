<?php


namespace EventBlock\QueryBuilder;

use Elastica\Query;
use Elastica\Query\BoolQuery;
use EventBlock\QueryFilter\BoolQueryFilterInterface;
use EventBlock\Sort\SortingInterface;

class ElasticaQueryBuilder
{
    /**
     * @var Query
     */
    public $query;

    public function __construct()
    {
        $this->query = new Query();
    }

    /**
     * @return Query
     */
    public function getQuery(): Query
    {
        return $this->query;
    }

    /**
     * @param BoolQueryFilterInterface[] $filters
     * @param array $aggregations
     * @param SortingInterface|null $sorting
     * @return Query
     */
    public function buildSearchQuery(
        array $filters = [],
        array $aggregations = [],
        ?SortingInterface $sorting = null
    ): Query {

        $stringQuery = new BoolQuery();

        $this->query->setQuery($stringQuery);
        foreach ($filters as $filter) {
            $filter->applyFilter($stringQuery);
        }
        foreach ($aggregations as $aggregation) {
            $this->query->addAggregation($aggregation);
        }
        // Move sorting to one class.
        if ($sorting) {
            $sorting->applySorting($this->query);
        }

        return $this->query;
    }
}